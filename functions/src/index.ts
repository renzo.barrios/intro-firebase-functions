import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
admin.initializeApp(functions.config().firebase);

const db = admin.firestore();

export const helloWorld = functions.https.onRequest(
  async (request, response) => {
    functions.logger.info("Hello logs!", { structuredData: true });
    response.send();
  }
);

export const addUser = functions.https.onRequest(async (request, response) => {
  // Obtenemos el nombre (name), edad (age), objetivo (target)
  const name = request.body.name;
  const age = request.body.age;
  const target = request.body.age;

  // Si no tenemos ningún parámetro la petición es incorrecta
  if (!name || !age || !target) {
    response.status(403).send("Invalid request body");
    return;
  }

  const user = {
    name,
    age,
    target,
  };

  // Guardamos en Firestore el usuario
  await db.collection("users").add(user);

  response.json({ res: "Usuario creado con éxito!" });
});

export const addRunningKM = functions.https.onRequest(
  async (request, response) => {
    // Obtenemos el nombre (name), edad (age), objetivo (target)
    const username = request.body.username;
    const km = request.body.km;

    // Si no tenemos ningún parámetro la petición es incorrecta
    if (!username || !km) {
      response.status(403).send("Invalid request body");
      return;
    }

    const data = {
      runningKilometers: km,
    };

    // Buscamos el usuario a actualizar
    const userId = (
      await db.collection("users").where("name", "==", username).limit(1).get()
    ).docs[0].id;

    if (!userId) {
      response.status(404).send("User not found");
      return;
    }

    // Guardamos en Firestore el lso kilómetros recorridos por el usuario
    await db.collection("users").doc(userId).update(data);

    response.json({ userId, km });
  }
);

export const getTopRunner = functions.https.onRequest(async (_, response) => {
  /*
    Buscamos todos los usuarios en nuestra base de datos e ordenamos de forma ascendente por el campo
    "runningKilometers"
  */
  const allRunners = (
    await db.collection("users").orderBy("runningKilometers").get()
  ).docs.map((doc) => ({
    ...doc.data(),
    id: doc.id,
  }));

  // Seleccionamos el ultimo del listado, este va a ser el de mayor kilómetros recorridos
  const topRunner = allRunners[allRunners.length - 1];

  response.json({ name: topRunner?.name, km: topRunner?.runningKilometers });
});
